#!/bin/bash
while ! exec 6<>/dev/tcp/config-service/8888; do
    echo "Trying to connect to config-service..."
    sleep 10
done
while ! exec 6<>/dev/tcp/auth-db/27017; do
    echo "Trying to connect to auth-db..."
    sleep 10
done
while ! exec 6<>/dev/tcp/registry-service/8761; do
    echo "Trying to connect to auth-db..."
    sleep 10
done

java  -agentlib:jdwp=transport=dt_socket,address=5001,server=y,suspend=n  -Xmx200m  -jar  /app/auth-service.jar
