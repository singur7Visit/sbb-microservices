#!/bin/bash
while ! exec 6<>/dev/tcp/config-service/8888; do
    echo "Trying to connect to config-service..."
    sleep 10
done
while ! exec 6<>/dev/tcp/train-mongodb/27017; do
    echo "Trying to connect to train-mongodb..."
    sleep 10
done
while ! exec 6<>/dev/tcp/hivemq/1883; do
    echo "Trying to connect to hive..."
    sleep 10
done

java -agentlib:jdwp=transport=dt_socket,address=6001,server=y,suspend=n -Xmx200m -jar /app/train-service.jar