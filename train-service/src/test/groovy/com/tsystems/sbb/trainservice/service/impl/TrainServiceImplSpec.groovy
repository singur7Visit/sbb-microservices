package com.tsystems.sbb.trainservice.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.tsystems.sbb.trainservice.client.RoutesClient

import com.tsystems.sbb.trainservice.domain.Train
import com.tsystems.sbb.trainservice.exception.TrainIsOffException
import com.tsystems.sbb.trainservice.facade.data.RouteData
import com.tsystems.sbb.trainservice.facade.data.ScheduleData
import org.junit.Test
import spock.lang.Specification

import java.time.LocalDateTime

class TrainServiceImplSpec extends Specification {

    @Collaborator
    RoutesClient routesClient = Mock()

    @Subject
    TrainServiceImpl trainService

    @Test
    "when availability of train is checked and train is off less than in 10 minutes - then throw exception"() {
        given:
        def train = new Train(routeId: "route")
        and:
        routesClient.getRoute("route") >> new RouteData(stationSchedules: ["station" : new ScheduleData(departure: LocalDateTime.now().plusMinutes(9))])
        when:
        trainService.checkAvailableTrainByStation(train, "station")
        then:
        thrown TrainIsOffException
    }
}
