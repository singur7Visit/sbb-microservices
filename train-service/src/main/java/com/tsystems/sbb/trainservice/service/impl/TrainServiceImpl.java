package com.tsystems.sbb.trainservice.service.impl;

import com.tsystems.sbb.trainservice.client.RoutesClient;
import com.tsystems.sbb.trainservice.domain.Train;
import com.tsystems.sbb.trainservice.exception.TrainIsOffException;
import com.tsystems.sbb.trainservice.facade.data.RouteData;
import com.tsystems.sbb.trainservice.facade.data.ScheduleData;
import com.tsystems.sbb.trainservice.repository.TrainRepository;
import com.tsystems.sbb.trainservice.service.TrainService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TrainServiceImpl implements TrainService {

    private static final Logger LOG = LoggerFactory.getLogger(TrainServiceImpl.class);

    private static final long minimumLeftMinutes = 10;
    @Autowired
    private TrainRepository trainRepository;
    @Autowired
    private RoutesClient routesClient;

    @Override
    public Train createTrain(Train train) {
        LOG.debug("TrainService: check available ticket for train {}", train.getNumber());
        return trainRepository.insert(train);
    }

    @Override
    public Train getTrainByNumber(String trainNumber) {
        return trainRepository.findByNumber(trainNumber);
    }

    @Override
    public void checkAvailableTrainByStation(Train train, String station) {
        LOG.debug("TrainService: check available train {} for station {}", train.getNumber(), station);
        RouteData route = routesClient.getRoute(train.getRouteId());
        ScheduleData schedule = route.getStationSchedules().get(station);
        long leftMinutes = LocalDateTime.now().until(schedule.getDeparture(), ChronoUnit.MINUTES);
        if (leftMinutes < minimumLeftMinutes)
            throw new TrainIsOffException();
    }

    @Override
    public List<Train> searchTrain(String departureStation, String arrivedStation, LocalDateTime startDate, LocalDateTime endDate) {
        LOG.debug("TrainService: search train between {} and {} , and between {} and {}", departureStation, arrivedStation, startDate, endDate);
        List<String> routeIds = routesClient.searchRoutes(departureStation, arrivedStation).stream()
                .filter(route -> filterByDate(route, departureStation, startDate, endDate))
                .map(RouteData::getId)
                .collect(Collectors.toList());
        return trainRepository.findByRouteIdIn(routeIds);
    }

    private boolean filterByDate(RouteData route, String departureStation, LocalDateTime startDate, LocalDateTime endDate) {
        LOG.debug("TrainService: filter available trains for station {} by time between {} and {} ", departureStation, startDate, endDate);
        LocalDateTime departureTime = route.getStationSchedules().get(departureStation).getDeparture();
        return departureTime.isAfter(LocalDateTime.now())
                && (departureTime.isAfter(startDate) || departureTime.isEqual(startDate))
                && (departureTime.isBefore(endDate) || departureTime.isEqual(endDate));
    }

    @Override
    public List<Train> getTrains(String station) {
        LOG.debug("TrainService: get trains for station {}", station);
        if(station == null) {
            return trainRepository.findAll();
        }
        else {
            List<RouteData> routes = routesClient.searchRoutes(station);
            return trainRepository.findByRouteIdIn(routes.stream().map(RouteData::getId).collect(Collectors.toList()));
        }
    }

    @Override
    public void update(Train train) {
        trainRepository.save(train);
    }

    @Override
    public Train getTrainById(String id) {
        return trainRepository.findOne(id);
    }

    @Override
    public List<Train> getTrainsByRoutes(List<String> routeIds) {
        return trainRepository.findByRouteIdIn(routeIds);
    }
}
