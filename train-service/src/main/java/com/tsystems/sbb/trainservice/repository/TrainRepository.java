package com.tsystems.sbb.trainservice.repository;

import com.tsystems.sbb.trainservice.domain.Train;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TrainRepository extends MongoRepository<Train,String> {
    Train findByNumber(String number);
    List<Train> findByRouteIdIn(Collection<String> routes);
}
