package com.tsystems.sbb.trainservice.client;

import com.tsystems.sbb.trainservice.facade.data.RouteData;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("station-service")
public interface RoutesClient {
    @RequestMapping(path = "/station-service/routes/{id}", method = RequestMethod.GET)
    RouteData getRoute(@PathVariable("id") String id);

    @RequestMapping(path = "/station-service/routes", method = RequestMethod.GET)
    List<RouteData> searchRoutes(@RequestParam("departureStation") String departureStation, @RequestParam("arriveStation") String arriveStation);


    @RequestMapping(path = "/station-service/routes", method = RequestMethod.GET)
    List<RouteData> searchRoutes(@RequestParam("station") String station);
}
