package com.tsystems.sbb.trainservice.facade.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.Min;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RouteData {
    private String id;

    @Min(2)
    private Map<String, ScheduleData> stationSchedules;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, ScheduleData> getStationSchedules() {
        return stationSchedules;
    }

    public void setStationSchedules(Map<String, ScheduleData> stationSchedules) {
        this.stationSchedules = stationSchedules;
    }
}
