package com.tsystems.sbb.trainservice.api;

import com.tsystems.sbb.trainservice.facade.TicketFacade;
import com.tsystems.sbb.trainservice.facade.TrainFacade;
import com.tsystems.sbb.trainservice.facade.data.TicketRegisterData;
import com.tsystems.sbb.trainservice.facade.data.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/ticket")
public class TrainTicketController {

    private static final Logger LOG = LoggerFactory.getLogger(TrainTicketController.class);

    @Autowired
    private TicketFacade ticketFacade;
    @Autowired
    private TrainFacade trainFacade;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseData checkAvailableTicketForTrain(@RequestBody @Valid TicketRegisterData ticketRegisterData) {
        LOG.debug("TrainTicketController: check available ticket for train {}", ticketRegisterData.getTrainNumber());
        return ticketFacade.checkAvailableTicketForTrain(ticketRegisterData);
    }

    @RequestMapping(path = "/places/{trainNumber}", method = RequestMethod.GET)
    public Integer places(@PathVariable("trainNumber") String trainNumber) {
        LOG.debug("TrainTicketController: get places count for train {} ", trainNumber);
        return trainFacade.getPlaceForTrain(trainNumber);
    }
}
