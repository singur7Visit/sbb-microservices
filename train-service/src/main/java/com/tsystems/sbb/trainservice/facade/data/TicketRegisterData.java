package com.tsystems.sbb.trainservice.facade.data;


import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class TicketRegisterData {
    @NotNull
    private String trainNumber;
    @NotNull
    private String station;

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
}
