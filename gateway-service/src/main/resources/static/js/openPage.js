function openPage(page) {
    var params = {};
    if(getOauthTokenFromStorage) {
        params["access_token"] = getOauthTokenFromStorage();
    }
    var url = [page, $.param(params)].join('?');

    window.open(url, "_self");
}