function requestOauthToken(username, password) {

    var success = false;

    $.ajax({
        url: 'auth/oauth/token',
        datatype: 'json',
        type: 'post',
        headers: {'Authorization': 'Basic YnJvd3Nlcjo='},
        async: false,
        data: {
            scope: 'ui',
            username: username,
            password: password,
            grant_type: 'password'
        },
        success: function (data) {
            $.cookie('access_token', data.access_token);
            success = true;
        },
        error: function () {
            removeOauthTokenFromStorage();
        }
    });

    return success;
}

function getOauthTokenFromStorage() {
    return $.cookie('access_token');
}

function removeOauthTokenFromStorage() {
    return $.removeCookie("access_token",{ path: '/' });
}

function logout() {
    $.ajax({
        url: '/auth/revoke-token',
        type: 'GET',
        headers: {'Authorization': 'Bearer ' + getOauthTokenFromStorage()},
        async: false,
        success: function (data) {
            removeOauthTokenFromStorage();
            location.href = "/"
        },
        error: function () {
        }
    });

}