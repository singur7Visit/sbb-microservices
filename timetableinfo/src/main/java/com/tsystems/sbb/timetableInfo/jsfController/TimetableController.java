package com.tsystems.sbb.timetableInfo.jsfController;

import com.tsystems.sbb.timetableInfo.PahoListenerInitializer;
import com.tsystems.sbb.timetableInfo.TimetableInfoLoader;
import com.tsystems.sbb.timetableInfo.dto.TrainTimetable;
import org.apache.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@ManagedBean
@RequestScoped
public class TimetableController {
    private static final Logger LOGGER = Logger.getLogger(TimetableController.class);

    @Inject
    TimetableInfoLoader timetableInfoLoader;

    @Inject
    PahoListenerInitializer pahoListenerInitializer;

    private String stationName;

    @PostConstruct
    public void init() {
        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        stationName = (String) req.getAttribute("station");
        try {
            if(!pahoListenerInitializer.getMqttClient().isConnected())
                pahoListenerInitializer.getMqttClient().connect();
            pahoListenerInitializer.getMqttClient().subscribe(stationName);
        } catch (MqttException e) {
            LOGGER.error(e,e);
        }
    }

    public String getStationName(){
        return stationName;
    }

    public Collection<TrainTimetable> getTrainTimetableList(){
        Collection<TrainTimetable> trainTables = timetableInfoLoader.getStationsTimetableStorage().get(stationName);
        if(trainTables == null || trainTables.isEmpty()) {
            timetableInfoLoader.init();
            trainTables = timetableInfoLoader.getStationsTimetableStorage().get(stationName);
            if(trainTables == null)
                trainTables = new ArrayList<>();
        }
        return trainTables.stream().filter(this::byToday).collect(Collectors.toList());
    }

    private boolean byToday(TrainTimetable trainTimetable) {
        LocalDate departure = LocalDateTime.parse(trainTimetable.getDepartureTime()).toLocalDate();
        LocalDate arrive = LocalDateTime.parse(trainTimetable.getArrivedTime()).toLocalDate();
        LocalDate now = LocalDate.now();
        return departure.isEqual(now) || arrive.isEqual(now);
    }

}
