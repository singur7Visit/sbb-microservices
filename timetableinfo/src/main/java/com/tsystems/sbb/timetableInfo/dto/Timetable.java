package com.tsystems.sbb.timetableInfo.dto;

public class Timetable {
    private String arrive;
    private String departure;

    private String status;

    public String getArrive() {
        return arrive;
    }

    public void setArrive(String arrive) {
        this.arrive = arrive;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Timetable timetable = (Timetable) o;

        if (!arrive.equals(timetable.arrive)) return false;
        if (!departure.equals(timetable.departure)) return false;
        return status.equals(timetable.status);
    }

    @Override
    public int hashCode() {
        int result = arrive.hashCode();
        result = 31 * result + departure.hashCode();
        result = 31 * result + status.hashCode();
        return result;
    }
}
