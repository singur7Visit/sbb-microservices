package com.tsystems.sbb.timetableInfo;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class Test {
    public static void main(String[] args) {
        try {
            MqttClient mqttClient= new MqttClient("tcp://localhost:1883", MqttClient.generateClientId());
            mqttClient.connect();
            MqttMessage mqttMessage = new MqttMessage();
            mqttMessage.setId(66);
            mqttClient.publish("Moscow", mqttMessage);
            mqttClient.close();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

}
