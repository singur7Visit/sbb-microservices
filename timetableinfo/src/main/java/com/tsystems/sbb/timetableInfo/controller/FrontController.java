package com.tsystems.sbb.timetableInfo.controller;

import com.tsystems.sbb.timetableInfo.dispatcher.Dispatcher;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FrontController extends HttpServlet {

    private static final Logger LOG = Logger.getLogger(FrontController.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    private void process(HttpServletRequest request, HttpServletResponse response) {
        Dispatcher dispatcher = new Dispatcher();
        try {
            dispatcher.dispatch(request, response);
        } catch (ServletException | IOException e) {
            LOG.error(e,e);
        }
    }
}
