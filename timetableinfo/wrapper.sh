#!/bin/bash
while ! exec 6<>/dev/tcp/station-service/7000; do
    echo "Trying to connect to station-service..."
    sleep 10
done
while ! exec 6<>/dev/tcp/train-service/6000; do
    echo "Trying to connect to train-service..."
    sleep 10
done

while ! exec 6<>/dev/tcp/hivemq/1883; do
    echo "Trying to connect to hive..."
    sleep 10
done

/opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0