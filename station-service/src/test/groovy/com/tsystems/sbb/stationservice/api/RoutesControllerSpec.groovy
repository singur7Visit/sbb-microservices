package com.tsystems.sbb.stationservice.api

import com.tsystems.sbb.stationservice.facade.RouteFacade
import com.tsystems.sbb.stationservice.facade.data.RouteData
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import static org.hamcrest.Matchers.*
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

class RoutesControllerSpec extends Specification {

    def routeFacade = Mock(RouteFacade)
    def routesController = new RoutesController(routeFacade: routeFacade)
    def mockMvc = standaloneSetup(routesController).build()

    @Test
    "when schedule has invalid order then validation exception is thrown"() {
        setup:
        def invalidRoute = "{\"stationSchedules\":" +
                    "{" +
                        "\"spb\":" +
                            "{\"arrive\":\"2017-05-25T11:11\",\"departure\":\"2017-05-26T12:12\",\"status\":\"OK\"}," +
                        "\"moscow\":" +
                            "{\"arrive\":\"2017-05-27T11:11\",\"departure\":\"2017-05-25T12:12\",\"status\":\"OK\"}}" +
                    "}" +
                "}"

        when:
        def response = mockMvc.perform(post('/routes')
                .contentType(APPLICATION_JSON)
                .content(invalidRoute)
        )

        then:
        0 * routeFacade.createRoute(_ as RouteData)
        response
                .andExpect(status().isInternalServerError())
                .andExpect(content().contentType(APPLICATION_JSON_UTF8))
                .andExpect(jsonPath('$.success', is(false)))
    }

    @Test
    "when schedule has valid order then validation exception is not thrown"() {
        given:
        def valid = "{\"stationSchedules\":" +
                    "{" +
                        "\"spb\":" +
                            "{\"arrive\":\"2017-05-25T11:11\",\"departure\":\"2017-05-26T12:12\",\"status\":\"OK\"}," +
                        "\"moscow\":" +
                            "{\"arrive\":\"2017-05-27T11:11\",\"departure\":\"2017-05-28T12:12\",\"status\":\"OK\"}}" +
                    "}" +
                "}"
        when:
            mockMvc.perform(post('/routes')
                    .contentType(APPLICATION_JSON)
                    .content(valid)
            )
        then:
        1 * routeFacade.createRoute(_ as RouteData)


    }
}
