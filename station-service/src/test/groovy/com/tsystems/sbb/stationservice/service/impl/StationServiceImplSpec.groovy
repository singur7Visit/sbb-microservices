package com.tsystems.sbb.stationservice.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.tsystems.sbb.stationservice.entity.Station
import com.tsystems.sbb.stationservice.repository.StationRepository
import org.junit.Test
import spock.lang.Specification

class StationServiceImplSpec extends Specification {

    @Collaborator
    StationRepository stationRepository = Mock()

    @Subject
    StationServiceImpl stationService

    @Test
    "when station is created - save in repository is called"() {
        given:
            def station = new Station()
        when:
            stationService.createStation(station)
        then:
        1 * stationRepository.save(station)
    }

}
