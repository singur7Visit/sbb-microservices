package com.tsystems.sbb.stationservice.facade.converter;

import com.tsystems.sbb.stationservice.entity.Schedule;
import com.tsystems.sbb.stationservice.facade.data.ScheduleData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ScheduleConverter implements Converter<Schedule, ScheduleData> {
    @Override
    public ScheduleData convert(Schedule source) {
        ScheduleData scheduleData = new ScheduleData();
        scheduleData.setArrive(source.getArrive());
        scheduleData.setDeparture(source.getDeparture());
        return scheduleData;
    }
}
