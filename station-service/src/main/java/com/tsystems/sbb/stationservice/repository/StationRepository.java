package com.tsystems.sbb.stationservice.repository;

import com.tsystems.sbb.stationservice.entity.Station;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StationRepository extends JpaRepository<Station, String> {

    Station findByName(String name);

    List<Station> findByNameIsLike(String name);

    void deleteByName(String name);
}
