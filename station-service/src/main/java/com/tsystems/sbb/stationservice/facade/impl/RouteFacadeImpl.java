package com.tsystems.sbb.stationservice.facade.impl;

import com.tsystems.sbb.stationservice.client.TrainServiceClient;
import com.tsystems.sbb.stationservice.entity.Route;
import com.tsystems.sbb.stationservice.facade.RouteFacade;
import com.tsystems.sbb.stationservice.facade.data.RouteData;
import com.tsystems.sbb.stationservice.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class RouteFacadeImpl implements RouteFacade {

    private static final String ARCHIVED = "ARCHIVED";
    @Autowired
    private RouteService routeService;
    @Autowired
    private Converter<Route, RouteData> routeConverter;
    @Autowired
    private Converter<RouteData, Route> routeDataConverter;
    @Autowired
    private TrainServiceClient trainServiceClient;

    @Override
    public RouteData getRouteById(String id) {
        return routeConverter.convert(routeService.find(id));
    }

    @Override
    public List<RouteData> getRoutesByStations(String departureStation, String arriveStation) {
        return routeService.getRoutesByStations(departureStation,arriveStation).stream()
                .filter(this::filterByStatus)
                .map(routeConverter::convert).collect(Collectors.toList());
    }

    @Override
    public void createRoute(RouteData routeData) {
        routeService.createRoute(routeDataConverter.convert(routeData));
    }

    @Override
    public List<RouteData> getRoutesByStation(String station) {
        return routeService.getRoutesByStation(station).stream()
                .filter(this::filterByStatus)
                .map(routeConverter::convert).collect(Collectors.toList());
    }

    @Override
    public List<RouteData> getRoutes(boolean filter) {
        Stream<Route> routes = getRoutes();
        if(filter)
            routes = routes.filter(this::filterByStatus);
        return routes.map(routeConverter::convert).collect(Collectors.toList());
    }

    @Override
    public void archiveRoute(String id) {
        Route route = routeService.getRouteById(id);
        route.setStatus(ARCHIVED);
        routeService.update(route);
        trainServiceClient.archiveByRouteId(id);
    }

    @Override
    public void archiveRouteByStation(String station) {
        List<Route> routesByStation = routeService.getRoutesByStation(station);
        routesByStation.stream().map(Route::getId).forEach(this::archiveRoute);
    }

    private Stream<Route> getRoutes() {
        return routeService.getRoutes().stream();
    }

    private boolean filterByStatus(Route route) {
        return !Objects.equals(route.getStatus(), ARCHIVED);
    }
}
