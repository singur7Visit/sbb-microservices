package com.tsystems.sbb.stationservice.facade.converter;

import com.tsystems.sbb.stationservice.entity.Route;
import com.tsystems.sbb.stationservice.entity.RouteEntry;
import com.tsystems.sbb.stationservice.entity.Schedule;
import com.tsystems.sbb.stationservice.entity.Station;
import com.tsystems.sbb.stationservice.facade.data.RouteData;
import com.tsystems.sbb.stationservice.facade.data.ScheduleData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class RouteConverter implements Converter<Route, RouteData> {

    @Autowired
    private Converter<Schedule, ScheduleData> scheduleDataConverter;

    @Override
    public RouteData convert(Route source) {
        RouteData routeData = new RouteData();
        routeData.setId(source.getId());
        routeData.setStationSchedules(source.getRouteEntries().stream()
                .collect(Collectors.toMap(routeEntry -> routeEntry.getStation().getName(),
                        routeEntry -> scheduleDataConverter.convert(routeEntry.getSchedule()),
                        (entry1, entry2) -> entry1,
                        LinkedHashMap::new)));
        routeData.setStatus(source.getStatus());
        return routeData;
    }


}
