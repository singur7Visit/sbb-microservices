package com.tsystems.sbb.stationservice.service.impl;

import com.tsystems.sbb.stationservice.entity.Station;
import com.tsystems.sbb.stationservice.repository.StationRepository;
import com.tsystems.sbb.stationservice.service.StationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

@Service
public class StationServiceImpl implements StationService {
    private static final Logger LOG = LoggerFactory.getLogger(StationServiceImpl.class);

    @Autowired
    private StationRepository stationRepository;

    @Override
    @Transactional
    public void createStation(Station station) {
        LOG.debug("StationService: {} save to repository", station.getName());
        stationRepository.save(station);
        LOG.debug("StationService: {} successfully saved to repository", station.getName());
    }

    @Override
    public List<Station> getStations(String station) {
        if (station == null) {
            LOG.debug("StationService: request all stations from repository");
            return stationRepository.findAll();
        } else {
            LOG.debug("StationService: request stations like {} from repository", station.toString());
            return stationRepository.findByNameIsLike(station);
        }
    }

    @Override
    public Station getStationByName(String name) {
        LOG.debug("StationService: request station {} from repository", name);
        return stationRepository.findByName(name);
    }

    @Override
    public Station getStationById(String id) {
        return stationRepository.findOne(id);
    }

    @Override
    public void updateStation(Station station) {
        stationRepository.save(station);
    }
}
