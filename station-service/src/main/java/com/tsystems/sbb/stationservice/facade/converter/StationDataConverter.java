package com.tsystems.sbb.stationservice.facade.converter;

import com.tsystems.sbb.stationservice.entity.Station;
import com.tsystems.sbb.stationservice.facade.data.StationData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StationDataConverter implements Converter<StationData, Station>{
    @Override
    public Station convert(StationData stationData) {
        Station station = new Station();
        station.setId(stationData.getId());
        station.setName(stationData.getName());
        station.setStatus(stationData.getStatus());
        return station;
    }
}
