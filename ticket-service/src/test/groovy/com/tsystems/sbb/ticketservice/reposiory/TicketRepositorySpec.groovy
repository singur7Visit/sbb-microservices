package com.tsystems.sbb.ticketservice.reposiory

import com.tsystems.sbb.ticketservice.entity.Passenger
import com.tsystems.sbb.ticketservice.entity.Ticket
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import java.time.LocalDate

@SpringBootTest
class TicketRepositorySpec extends Specification {
    @Autowired
    TicketRepository ticketRepository

    def setup() {
        ticketRepository.deleteAll()
    }

    @Test
    "when then"() {
        given:
        Passenger passenger = new Passenger(name: "s", surname: "d", birthday: LocalDate.now())
        Ticket ticket = new Ticket(trainNumber: "1234", passenger: passenger)
        when:
        ticketRepository.save(ticket)
        then:
        ticket.getNumber()
    }
}
