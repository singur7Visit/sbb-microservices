package com.tsystems.sbb.ticketservice.api

import com.tsystems.sbb.ticketservice.facade.TicketFacade
import com.tsystems.sbb.ticketservice.facade.data.TicketData
import com.tsystems.sbb.ticketservice.facade.data.TicketResponseData
import org.junit.Test
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification


import static org.hamcrest.Matchers.*
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath

class TicketControllerSpec extends Specification {

    def ticketFacade = Mock(TicketFacade)
    TicketController target = new TicketController(ticketFacade: ticketFacade)

    def mockMvc  = MockMvcBuilders.standaloneSetup(target).build()

    @Test
    "when ticket is created - then response has ticket number"() {
        setup:
        def actualTicket = new File('src/test/resources/ticket-data.json').text

        when:
        def response = mockMvc.perform(post('/')
                .contentType(APPLICATION_JSON)
                .content(actualTicket)
        )

        then:
        1 * ticketFacade.createTicket(_ as TicketData) >> {
            new TicketResponseData(ticketNumber: "000")
        }
        response
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON_UTF8))
            .andExpect(jsonPath('$.ticketNumber', is("000")))
    }

}
