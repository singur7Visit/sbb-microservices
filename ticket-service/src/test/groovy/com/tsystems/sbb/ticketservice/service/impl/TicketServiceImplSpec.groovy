package com.tsystems.sbb.ticketservice.service.impl

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import com.tsystems.sbb.ticketservice.client.TrainServiceClient
import com.tsystems.sbb.ticketservice.client.data.TrainClientResponse
import com.tsystems.sbb.ticketservice.entity.Passenger
import com.tsystems.sbb.ticketservice.entity.Ticket
import com.tsystems.sbb.ticketservice.facade.data.TicketRegisterData
import com.tsystems.sbb.ticketservice.reposiory.TicketRepository
import com.tsystems.sbb.ticketservice.service.PassengerService
import org.junit.Test
import spock.lang.Specification


class TicketServiceImplSpec extends Specification {

    @Collaborator
    TicketRepository ticketRepository = Mock()
    @Collaborator
    PassengerService passengerService = Mock()

    @Subject
    TicketServiceImpl ticketService

    @Test
    "when ticket is created and ticket with same passenger already exist - then throw exception"() {
        given:
        def passenger = new Passenger()
        passenger.setId("passenger")
        def ticket = new Ticket(trainNumber: "001", passenger: passenger)
        and:
        ticketRepository.existsByTrainNumberAndPassenger("001", passenger) >> true

        when:
        ticketService.createTicket(ticket)

        then:
        thrown IllegalArgumentException
    }

    @Test
    "when ticket is valid - then ticket is saved"() {
        given:
        def passenger = Mock(Passenger)
        def ticket = new Ticket(trainNumber: "001", passenger: passenger)
        and:
        ticketRepository.existsByTrainNumberAndPassenger("001", passenger) >> true
        when:
        ticketService.createTicket(ticket)
        then:
        1 * ticketRepository.save(ticket)
    }


}
