package com.tsystems.sbb.ticketservice.service.impl;

import com.tsystems.sbb.ticketservice.api.TicketController;
import com.tsystems.sbb.ticketservice.entity.Passenger;
import com.tsystems.sbb.ticketservice.entity.Ticket;
import com.tsystems.sbb.ticketservice.reposiory.TicketRepository;
import com.tsystems.sbb.ticketservice.service.PassengerService;
import com.tsystems.sbb.ticketservice.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    private static final Logger LOG = LoggerFactory.getLogger(TicketController.class);

    @Autowired
    private TicketRepository ticketRepository;
    @Autowired
    private PassengerService passengerService;

    @Transactional
    @Override
    public Ticket createTicket(Ticket ticket) {
        checkExistingPassengers(ticket);
        return ticketRepository.save(ticket);
    }

    @Override
    public List<Ticket> getTicketForTrainNumber(String trainNumber) {
        LOG.debug("TicketService: get tickets for train {}", trainNumber);
        return ticketRepository.findByTrainNumber(trainNumber);
    }

    @Override
    public int getCountSoldTicketsForTrain(String trainNumber) {
        LOG.debug("TicketService: get count of sold tickets fo {}", trainNumber);
        return ticketRepository.countAllByTrainNumber(trainNumber);
    }

    private void checkExistingPassengers(Ticket ticket) {
        LOG.debug("TicketService: check passenger {}", ticket.getPassenger());
        Passenger passenger = null;
        if (ticket.getPassenger().getId() == null) {
            passenger = passengerService.refresh(ticket.getPassenger());
            if (passenger != null) {
                ticket.setPassenger(passenger);
            }
        }
        if (ticket.getPassenger().getId() != null) {
            if (ticketRepository.existsByTrainNumberAndPassenger(ticket.getTrainNumber(), ticket.getPassenger())) {
                throw new IllegalArgumentException("Passenger already registered");
            }
        }
    }
}