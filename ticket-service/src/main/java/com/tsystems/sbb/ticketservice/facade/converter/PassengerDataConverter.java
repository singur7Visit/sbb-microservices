package com.tsystems.sbb.ticketservice.facade.converter;

import com.tsystems.sbb.ticketservice.entity.Passenger;
import com.tsystems.sbb.ticketservice.facade.data.PassengerData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PassengerDataConverter implements Converter<PassengerData, Passenger> {

    @Override
    public Passenger convert(PassengerData passengerData) {
        Passenger passenger = new Passenger();
        passenger.setName(passengerData.getName());
        passenger.setSurname(passengerData.getSurname());
        passenger.setBirthday(passengerData.getBirthday());
        return passenger;
    }
}
