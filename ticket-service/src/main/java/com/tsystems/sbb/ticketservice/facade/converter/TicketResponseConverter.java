package com.tsystems.sbb.ticketservice.facade.converter;

import com.tsystems.sbb.ticketservice.entity.Ticket;
import com.tsystems.sbb.ticketservice.facade.data.TicketResponseData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TicketResponseConverter implements Converter<Ticket, TicketResponseData> {
    @Override
    public TicketResponseData convert(Ticket ticket) {
        TicketResponseData ticketResponseData = new TicketResponseData();
        ticketResponseData.setTicketNumber(ticket.getNumber());
        return ticketResponseData;
    }
}
