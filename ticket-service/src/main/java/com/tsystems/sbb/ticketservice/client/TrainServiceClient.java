package com.tsystems.sbb.ticketservice.client;

import com.tsystems.sbb.ticketservice.client.data.TrainClientResponse;
import com.tsystems.sbb.ticketservice.facade.data.TicketRegisterData;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("train-service")
public interface TrainServiceClient {

    /**
     * Sends request for train-service for ticket registration
     * @param ticketRegisterData - ticket register data
     * @return train-service response
     */
    @RequestMapping(method = RequestMethod.POST, value = "/train-service/ticket")
    TrainClientResponse checkAvailableTicketForTrain(@RequestBody TicketRegisterData ticketRegisterData);

    @RequestMapping(method = RequestMethod.GET, value = "/train-service/ticket/places/{trainNumber}")
    @Cacheable("places")
    Integer getPlacesForTrain(@PathVariable("trainNumber") String trainNumber);
}
