package com.tsystems.sbb.ticketservice.facade.converter;

import com.tsystems.sbb.ticketservice.facade.data.TicketData;
import com.tsystems.sbb.ticketservice.facade.data.TicketRegisterData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class TicketRegisterConverter implements Converter<TicketData, TicketRegisterData> {

    @Override
    public TicketRegisterData convert(TicketData ticketData) {
        TicketRegisterData ticketRegisterData = new TicketRegisterData();
        ticketRegisterData.setTrainNumber(ticketData.getTrainNumber());
        ticketRegisterData.setStation(ticketData.getStation());
        return ticketRegisterData;
    }
}
