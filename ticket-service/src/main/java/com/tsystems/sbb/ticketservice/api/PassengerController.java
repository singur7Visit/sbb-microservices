package com.tsystems.sbb.ticketservice.api;

import com.tsystems.sbb.ticketservice.facade.PassengerFacade;
import com.tsystems.sbb.ticketservice.facade.data.PassengerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/passengers")
public class PassengerController extends BaseController{
    private static final Logger LOG = LoggerFactory.getLogger(PassengerController.class);

    @Autowired
    private PassengerFacade passengerFacade;

    @RequestMapping(method = RequestMethod.GET)
    @Secured("ROLE_MANAGER")
    public List<PassengerData> getPassengers(@RequestParam("train") String trainNumber) {
        LOG.debug("PassengerController: get passengers for train number {} request accepted", trainNumber);
        return passengerFacade.getPassengersForTrain(trainNumber);
    }
}
