package com.tsystems.sbb.ticketservice.facade.data;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class TicketData {
    @NotNull
    @Pattern(regexp = ".*[\\S]+.*")
    private String trainNumber;
    @NotNull @Valid
    private PassengerData passenger;
    @NotNull
    @Pattern(regexp = ".*[\\S]+.*")
    private String station;

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public PassengerData getPassenger() {
        return passenger;
    }

    public void setPassenger(PassengerData passenger) {
        this.passenger = passenger;
    }

    public String getStation() {
        return station;
    }

    public void setStation(String station) {
        this.station = station;
    }
}
