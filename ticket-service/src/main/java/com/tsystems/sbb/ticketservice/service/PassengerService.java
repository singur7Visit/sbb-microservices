package com.tsystems.sbb.ticketservice.service;

import com.tsystems.sbb.ticketservice.entity.Passenger;

public interface PassengerService {

    /**
     * Refreshes given passenger from repository
     *
     * @param passenger - passenger
     */
    Passenger refresh(Passenger passenger);
}
