package com.tsystems.sbb.ticketservice.facade.converter;

import com.tsystems.sbb.ticketservice.entity.Passenger;
import com.tsystems.sbb.ticketservice.facade.data.PassengerData;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class PassengerConverter implements Converter<Passenger, PassengerData>{
    @Override
    public PassengerData convert(Passenger passenger) {
        PassengerData passengerData = new PassengerData();
        passengerData.setName(passenger.getName());
        passengerData.setSurname(passenger.getSurname());
        passengerData.setBirthday(passenger.getBirthday());
        return passengerData;
    }
}
