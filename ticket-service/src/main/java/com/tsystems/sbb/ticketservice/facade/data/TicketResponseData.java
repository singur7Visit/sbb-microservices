package com.tsystems.sbb.ticketservice.facade.data;

import javax.validation.constraints.NotNull;

public class TicketResponseData {
    @NotNull
    private String ticketNumber;

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }
}
